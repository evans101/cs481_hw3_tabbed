﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3_Tabbed
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TabbedPage1 : TabbedPage
    {
        //display assignments that were completed
        ObservableCollection<Assignment> dueCompleted = new ObservableCollection<Assignment>();
        public ObservableCollection<Assignment> dueCompletedDisplay { get { return dueCompleted; } }
        //display assignments due today
        ObservableCollection<Assignment> dueToday = new ObservableCollection<Assignment>();
        public ObservableCollection<Assignment> dueTodayDisplay { get { return dueToday; } }
        //display assignments upcoming
        ObservableCollection<Assignment> dueUpcoming = new ObservableCollection<Assignment>();
        public ObservableCollection<Assignment> dueUpcomingDisplay { get { return dueUpcoming; } }
        //sets today to current time
        public DateTime today = DateTime.Now;
        int todayIndex = -1, upcomingIndex = -1;

        public TabbedPage1()
        {
            InitializeComponent();

        }
        //adds assignments and determines whether it is due today or upcoming
        private void AddAssignment_Clicked(object sender, EventArgs e)
        {
            string assignment = ((Entry)assignmentName).Text;
            string course = ((Entry)courseName).Text;
            DateTime time = ((DatePicker)dueDate).Date;
            if (assignment != "" && course != "")
            {
                if(time.Date.Month == today.Date.Month && time.Date.Day == today.Date.Day)
                {
                    todayIndex++;
                    dueToday.Add(new Assignment { Name = assignment, Course = course, DueDate = time, TodayIndex = todayIndex });
                    assignmentName.Text = "";
                    courseName.Text = "";
                }
                else
                {

                    upcomingIndex++;
                    dueUpcoming.Add(new Assignment { Name = assignment, Course = course, DueDate = time, UpcomingIndex = upcomingIndex });
                    assignmentName.Text = "";
                    courseName.Text = "";
                }

            }
        }
        //completes assignment that was due today
        private void CompleteAssignmentToday_Clicked(object sender, EventArgs e)
        {
            Assignment assignmentCompleted = dueToday[todayIndex];
            dueCompleted.Add(assignmentCompleted);
            dueToday.Remove(assignmentCompleted);
            todayIndex--;
        }
        //completes assignment that was due upcoming
        private void CompleteAssignmentUpcoming_Clicked(object sender, EventArgs e)
        {
            Assignment assignmentCompleted = dueUpcoming[upcomingIndex];
            dueCompleted.Add(assignmentCompleted);
            dueUpcoming.Remove(assignmentCompleted);
            upcomingIndex--;
        }

        private void OnAppear(object sender, EventArgs e)
        {
            dueCompletedList.ItemsSource = dueCompleted;
            dueTodayList.ItemsSource = dueToday;
            dueUpcomingList.ItemsSource = dueUpcoming;
        }
        private void OnDisappear(object sender, EventArgs e)
        {
            dueCompleted.Clear();
            dueUpcoming.Clear();
            dueToday.Clear();
        }
    }
}